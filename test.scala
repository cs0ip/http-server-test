#!/bin/bash
exec scala -howtorun:script -savecompiled "$0" $@
!#

import java.io._
import java.net.{ServerSocket, Socket}
import java.nio.charset.{Charset, StandardCharsets}
import java.util.concurrent.atomic.AtomicInteger
import java.util.regex.Pattern
import java.util.zip.{ZipEntry, ZipFile}

import scala.util.control.NonFatal
import scala.util.{Failure, Success, Try}

object Main extends App {
  try {
    if (args.length < 1)
      throw new Exception("you must specify a zip-file with images as first argument")
    val file = new File(args(0))
    val storage = ImageStorage(file)
    val handlers = Seq(
      new Handlers.Stats.Factory(storage),
      new Handlers.GetImage.Factory(storage)
    )
    val server = new Server(8080, handlers)
    server.start()
    Utils.close(storage)
  } catch {
    case NonFatal(e) => Utils.printException(e)
  }
}

class Server(port: Int, handlers: Seq[RequestHandlerFactory]) {
  import Server._

  def start(): Unit = {
    val accepterThread = new AccepterThread(port, handlers)
    accepterThread.start()
    accepterThread.join()
  }
}

object Server {
  private class AccepterThread(port: Int, handlers: Seq[RequestHandlerFactory]) extends Thread("accepter-thread") {
    override def run(): Unit = {
      val serverSocket = new ServerSocket(8080)
      println("server started")
      var clientTreadId = 1
      try {
        while (!isInterrupted) {
          val clientSocket: Socket =
            try {
              serverSocket.accept()
            } catch {
              case _: InterruptedException => null
            }
          if (clientSocket != null) {
            clientSocket.setSoLinger(true, 300)
            val clientThread = new ClientThread(
              clientTreadId,
              clientSocket,
              handlers
            )
            clientTreadId += 1
            clientThread.start()
          }
        }
      } finally {
        serverSocket.close()
        println("server stopped")
      }
    }
  }

  private class ClientThread(id: Int, socket: Socket, handlers: Seq[RequestHandlerFactory])
    extends Thread(s"client-thread-$id")
  {
    import ClientThread._

    private val input = socket.getInputStream
    private val output = socket.getOutputStream
    private val lineReader = new LineReader(input)
    private val requestReader = new RequestReader(id, lineReader)

    override def run(): Unit = {
      Utils.printClientMsg(id, "connected")
      try {
        processRequest() match {
          case Success(response) => writeResponse(response)
          case Failure(e) => e match {
            case _: ConnectionInterruptedException => ()

            case e: Http.NotFoundException =>
              Utils.printClientMsg(id, e.getMessage)
              writeResponse(Response.Text(Http.Code.NotFound, e.getMessage))

            case e =>
              Utils.printClientException(id, e)
              writeResponse(Response.Text(Http.Code.Error, e.getMessage))
          }
        }
      } finally {
        input.skip(input.available())
        socket.close()
        Utils.printClientMsg(id, "disconnected")
      }
    }

    private def processRequest(): Try[Response] = {
      try {
        for {
          request <- requestReader.readRequest()
          handler <- routeRequest(request.path)
        } yield handler.exec()
      } catch {
        case NonFatal(e) => Failure(e)
      }
    }

    private def routeRequest(path: String): Try[RequestHandler] = {
      val handler = handlers.iterator
        .map(_.createIfPathMatches(path))
        .find(_.isDefined)
        .flatten

      handler match {
        case Some(handler) =>  Success(handler)
        case None => Failure(new Http.NotFoundException(s"resource handler not found; path = $path"))
      }
    }

    private def writeResponse(response: Response): Unit = {
      val b = new StringBuilder
      b.append(s"HTTP/1.0 ${response.code.value} ${response.code.name}").append(Http.RN)

      val bytes = response.body.bytes
      val contentLength = Http.Header.ContentLength(bytes.length)

      for ((_, header) <- response.headers if header.name != contentLength.name)
        b.append(s"${header.name}: ${header.strValue}").append(Http.RN)

      b.append(s"${contentLength.name}: ${contentLength.strValue}").append(Http.RN)

      val connection = Http.Header.Connection.Close
      b.append(s"${connection.name}: ${connection.value}").append(Http.RN)

      b.append(Http.RN)

      output.write(b.toString().getBytes(StandardCharsets.US_ASCII))

      if (bytes.nonEmpty)
        output.write(bytes)

      output.flush()
    }
  }

  private object ClientThread {

    class LineReader(input: InputStream) {
      private val buf = Array.ofDim[Byte](256)
      private var bufPosition: Int = 0
      private var bufSize: Int = 0
      private var charset = StandardCharsets.US_ASCII

      def readLine(): Option[String] = {
        val line = new ByteArrayOutputStream(64)
        var isR = false
        var isN = false
        var isStop = false
        while (!isN && !isStop) {
          while (!isN && bufPosition < bufSize) {
            val byte = buf(bufPosition)
            line.write(byte)
            if (byte == '\r')
              isR = true
            else if (isR && byte == '\n')
              isN = true
            else
              isR = false
            bufPosition += 1
          }
          if (!isN) {
            if (! readToBuffer())
              isStop = true
          }
        }
        if (isN)
          Some(new String(line.toByteArray, 0, line.size - 2, charset))
        else
          None
      }

      def setCharset(cs: Charset): Unit =
        charset = cs

      def skipAvailable(): Unit = {
        input.skip(input.available())
        bufSize = 0
        bufPosition = 0
      }

      private def readToBuffer(): Boolean = {
        val cnt = input.read(buf)
        bufSize = if (cnt < 0) 0 else cnt
        bufPosition = 0
        cnt >= 0
      }
    }

    class ConnectionInterruptedException(msg: String = null) extends RuntimeException(msg)

    class RequestReader(clientId: Int, reader: LineReader) {
      def readRequest(): Try[Request] = {
        reader.readLine() match {
          case Some(str) =>
            Utils.printClientMsg(clientId, s"readLine: $str")
            val res = readMethod(str).map(path => Request(path))
            reader.skipAvailable()
            res
          case None =>
            Failure(new ConnectionInterruptedException("connection interrupted"))
        }
      }

      private def readMethod(str: String): Try[String] = {
        val ar = str.split(' ')
        if (ar.length != 3)
          return fail("incorrect request")
        if (ar(0) != "GET")
          return fail("unsupported method")
        if (ar(1).isEmpty)
          return fail("path is empty")
        Success(ar(1))
      }

      private def fail(msg: String): Failure[Nothing] =
        Failure(new Exception(msg))
    }

    case class Request(path: String)
  }
}

trait RequestHandlerFactory {
  def createIfPathMatches(path: String): Option[RequestHandler]
}

trait RequestHandler {
  def exec(): Response
}

case class Response(
  code: Http.Code,
  headers: Map[String, Http.Header],
  body: Http.Body
)

object Response {
  def Text(code: Http.Code = Http.Code.Ok, text: String = "", cs: Charset = StandardCharsets.UTF_8): Response = {
    val contentType = Http.Header.ContentType.Text(cs)
    Response(
      code,
      Map(contentType.name -> contentType),
      Http.StringBody(text, cs)
    )
  }

  def Html(code: Http.Code = Http.Code.Ok, body: String = "", cs: Charset = StandardCharsets.UTF_8): Response = {
    val contentType = Http.Header.ContentType.Html(cs)
    val content = s"""
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="${cs.name()}">
        </head>
        <body>
          $body
        </body>
      </html>
    """
    Response(
      code,
      Map(contentType.name -> contentType),
      Http.StringBody(content, cs)
    )
  }

  def Jpeg(body: Http.ZipEntryBody): Response = {
    val contentType = Http.Header.ContentType.Jpeg
    Response(
      Http.Code.Ok,
      Map(contentType.name -> contentType),
      body
    )
  }
}

object Http {
  val RN = "\r\n"

  case class Code(value: Int, name: String) {
    def toHttpString: String = s"$value $name"
  }

  object Code {
    val Ok = Code(200, "OK")
    val NotFound = Code(404, "Not Found")
    val Error = Code(500, "Internal Server Error")
  }

  trait Header {
    def name: String
    def strValue: String
  }

  object Header {

    case class ContentType(value: String, cs: Option[Charset] = None) extends Header {
      override def name: String = ContentType.Name

      override def strValue: String = cs match {
        case Some(cs) => s"$value; charset=${cs.name()}"
        case None => value
      }
    }

    object ContentType {
      val Name = "Content-Type"

      def Text(cs: Charset = StandardCharsets.UTF_8): ContentType =
        ContentType("text/plain", Some(cs))

      def Html(cs: Charset = StandardCharsets.UTF_8): ContentType =
        ContentType("text/html", Some(cs))

      val Jpeg = ContentType("image/jpeg")
    }

    case class ContentLength(value: Int) extends Header {
      override def name: String = ContentLength.Name

      override def strValue: String = value.toString
    }

    object ContentLength {
      val Name = "Content-Length"
    }

    case class Connection(value: String) extends Header {
      override def name: String = Connection.Name

      override def strValue: String = value
    }

    object Connection {
      val Name = "Connection"

      val Close = Connection("close")
    }
  }

  trait Body {
    lazy val bytes: Array[Byte] = getBytes
    protected def getBytes: Array[Byte]
  }

  class StringBody private (val str: String, val cs: Charset) extends Body {
    override protected def getBytes: Array[Byte] = str.getBytes(cs)
  }

  object StringBody {
    def apply(str: String, cs: Charset = StandardCharsets.UTF_8): StringBody =
      new StringBody(
        if (str == null) "" else str,
        cs
      )
  }

  class ZipEntryBody private (file: ZipFile, entry: ZipEntry) extends Body {
    override protected def getBytes: Array[Byte] = {
      val stream = file.getInputStream(entry)
      try {
        val res = new ByteArrayOutputStream()
        val buf = new Array[Byte](256)
        var cnt = 0
        while ({cnt = stream.read(buf); cnt >= 0})
          res.write(buf, 0, cnt)
        res.toByteArray
      } finally {
        stream.close()
      }
    }
  }

  object ZipEntryBody {
    def apply(file: ZipFile, entry: ZipEntry): ZipEntryBody = new ZipEntryBody(file, entry)
  }

  class NotFoundException(msg: String = null) extends RuntimeException(msg)
}

class ImageStorage private (zipFile: ZipFile) extends AutoCloseable {
  import ImageStorage._

  private val entries: Map[Int, ZipEntry] = getEntries

  private val stats: Map[Int, AtomicInteger] = entries.map {
    case (id, _) => id -> new AtomicInteger(0)
  }

  def getImage(id: Int): Option[Http.ZipEntryBody] =
    for (entry <- entries.get(id))
      yield {
        stats(id).incrementAndGet()
        Http.ZipEntryBody(zipFile, entry)
      }

  def getStats: Vector[StatEntry] =
    stats.iterator
      .map(entry => StatEntry(entry._1, entry._2.get()))
      .filter(_.count > 0)
      .toVector
      .sortWith {
        case (x, y) =>
          val countCmp = Integer.compare(y.count, x.count)
          val res = if (countCmp == 0) Integer.compare(x.id, y.id) else countCmp
          res < 0
      }

  override def close(): Unit = zipFile.close()

  private def getEntries: Map[Int, ZipEntry] = {
    val builder = Map.newBuilder[Int, ZipEntry]
    val zipEntries = zipFile.entries()
    val pattern = Pattern.compile("""cat([0-9]+)\.jpg""", Pattern.CASE_INSENSITIVE)
    while (zipEntries.hasMoreElements) {
      val entry = zipEntries.nextElement()
      if (!entry.isDirectory) {
        val matcher = pattern.matcher(entry.getName)
        if (matcher.matches()) {
          val idStr = matcher.group(1)
          for (id <- Try(Integer.parseInt(idStr)) if id >= 0) {
            builder += (id -> entry)
            println(s"image with id = $id indexed")
          }
        }
      }
    }
    builder.result()
  }
}

object ImageStorage {
  def apply(file: File): ImageStorage = {
    val zipFile = new ZipFile(file)
    new ImageStorage(zipFile)
  }

  case class StatEntry(id: Int, count: Int)
}

object Handlers {
  object Stats {
    class Handler(storage: ImageStorage) extends RequestHandler {
      import ImageStorage.StatEntry

      override def exec(): Response = {
        val b = new StringBuilder
        val stats = storage.getStats
        writeStats(b, stats)
        Response.Html(body = b.toString())
      }

      private def writeStats(b: StringBuilder, stats: Vector[StatEntry]): Unit = {
        b.append("<table>").append('\n')

        b.append("<tr>").append('\n')
        b
          .append("""<th align="center">""").append("ID").append("</th>").append('\n')
          .append("""<th align="center" style="padding-left: 30px;">""").append("Count").append("</th>").append('\n')
        b.append("</tr>").append('\n')

        for (entry <- stats)
          writeEntry(b, entry)

        b.append("</table>").append('\n')
      }

      private def writeEntry(b: StringBuilder, entry: StatEntry): Unit = {
        b.append("<tr>").append('\n')
        b
          .append("""<td align="right">""").append(entry.id).append("</td>").append('\n')
          .append("""<td align="right" style="padding-left: 30px;">""").append(entry.count).append("</td>").append('\n')
        b.append("</tr>").append('\n')
      }
    }

    class Factory(storage: ImageStorage) extends RequestHandlerFactory {
      override def createIfPathMatches(path: String): Option[RequestHandler] = {
        if (path == "/stats")
          Some(new Handler(storage))
        else
          None
      }
    }
  }

  object GetImage {
    class Handler(storage: ImageStorage, id: Int) extends RequestHandler {
      override def exec(): Response = {
        storage.getImage(id) match {
          case Some(zipEntryBody) =>
            Response.Jpeg(zipEntryBody)
          case None =>
            Response.Text(Http.Code.NotFound, s"image with id = $id not found")
        }
      }
    }

    class Factory(storage: ImageStorage) extends RequestHandlerFactory {

      private val pattern = Pattern.compile("""^/image/([0-9]+)(?:/?)?.*$""")

      override def createIfPathMatches(path: String): Option[RequestHandler] = {
        val matcher = pattern.matcher(path)
        if (matcher.matches()) {
          try {
            val id = Integer.parseInt(matcher.group(1))
            require(id >= 0)
            Some(new Handler(storage, id))
          } catch {
            case NonFatal(_) => None
          }
        } else
          None
      }
    }
  }
}

object Utils {
  def close(closeable: AutoCloseable): Unit = {
    try {
      closeable.close()
    } catch {
      case NonFatal(e) => printException(e)
    }
  }

  def printException(e: Throwable): Unit = {
    println(exceptionToString(e))
  }

  def printClientException(id: Int, e: Throwable): Unit = {
    val msg = s"${clientMsg(id, "exception:")}\n${exceptionToString(e)}"
    println(msg)
  }

  def printError(msg: String): Unit =
    printException(new RuntimeException(msg))

  def printClientError(id: Int, msg: String): Unit =
    printException(new RuntimeException(clientMsg(id, msg)))

  def printClientMsg(id: Int, msg: String): Unit =
    println(clientMsg(id, msg))

  def clientMsg(id: Int, msg: String): String =
    s"client[$id]: $msg"

  def exceptionToString(e: Throwable): String = {
    val res = new ByteArrayOutputStream()
    val printStream = new PrintStream(res, true, StandardCharsets.UTF_8.name())
    e.printStackTrace(printStream)
    res.toString(StandardCharsets.UTF_8.name())
  }
}
